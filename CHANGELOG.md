# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.3] - 2021-04-28

### Removed

- VSCode snippets will be moved to the [RDMO extension]:
  - Behavior seems inconsistent for JSON and YAML files,
    some JSON constructs like object arrays don't work well in YAML.
  - Packaging snippets as part of the extension remains convenient
    when they're maintained in YAML and compiled to JSON,
    because YAML has powerful [multiline strings].
  - The [defaultSnippets] keyword is not part of the [JSON schema specification],
    but a VSCode contribution.

[RDMO extension]: https://gitlab.com/rdmo-json/vscode-extension
[JSON schema specification]: https://json-schema.org/specification.html
[defaultSnippets]: https://code.visualstudio.com/Docs/languages/json
[multiline strings]: https://yaml-multiline.info

## [0.8.2] - 2021-04-16

### Changed

- Updated deps
- Upgraded ajv to v8

## [0.8.1] - 2021-02-08

### Added

- Optional unit property for numeric response config
- Updated the VSCode snippets for question types to reflect new question settings

## [0.8.0] - 2021-02-03

### Added

#### More question types

- Yes/no question
- Single- and multiple-response question
  - Textual response (can be multiline)
  - Numeric response (can have range)
  - Date response
- Single-choice question (can be radio buttons or dropdown style)
- Multiple-choice question (can be checkboxes or dropdown style)

### Removed

- Old question types

## [0.7.0] - 2021-01-22

### Added

- Property `version` in catalog (must conform to the [SemVer] specification)

[SemVer]: https://semver.org

## [0.6.0] - 2021-01-22

### Added

#### Snippets for VSCode intellisense

Snippets have an unilingual and a multilingual version if appropriate.

- Domain entity
- Minimal catalog
- Section
- Questionset
- Questions
  - Single-choice question
  - Multiple-choice question
  - Open question
  - Yes/no question
- Option

### Fixed

- Downgraded to JSON schema draft-07
  which is better supported by VSCode and typescript generation

## [0.5.1] - 2021-01-20

### Changed

#### Domain schema

- A domain is a list of attributes and entities
- An attribute is a string containing a key
- An entity is an object with one property,
where the key denotes the entity key, and the value the children domain

## [0.5.0] - 2021-01-18

### Changed

#### Domain subschema refined

Domain subschema has been redesigned as domain tree.
Comments have been removed from attributes.
Each node can have the following shapes:

- One single key as string
- An array of keys
- An object where the keys denote attribute keys
  referring to domain trees as child attributes

- Attributes became mandatory for questions

### Added

- Questionset and question schema have got a help property

## [0.4.2] - 2021-01-16

### Fixed

#### Schema

- Closed questions now have a conditions array like questionsets and open questions

## [0.4.1] - 2021-01-16

### Fixed

- Include correct index.d.ts which was accidentally ignored from git

## [0.4.0] - 2021-01-16

### Added

#### Schema

- Condition subschema (a tuple of source, relation, target type, and target)
- Questionsets and questions can have an array of conditions

## [0.3.3] - 2021-01-14

### Added

- Option type is now exported in types

## [0.3.2] - 2021-01-14

### Fixed

- Types are exported as ajv types for smaller bundle size and better correctness

## [0.3.1] - 2021-01-10

### Added

- Typescript declarations

## [0.3.0] - 2021-01-10

### Changed

- Repo has moved to gitlab

### Added

- Allow $schema property in instances

## [0.2.0] - 2020-12-22

### Added

- Attribute property in questionset and question schema
- comment property

### changed

- Sections cannot contain questions anymore, only questionsets
- Keys can contain numbers

## [0.1.2] - 2020-12-20

### Fixed

- Build issue  with installation from github

## [0.1.1] - 2020-12-20

### Changed

#### Schema: Translated text

The `de` and `en` properties are now hardcoded in the schema  
instead of using the language code pattern  
because these two are needed and used by RDMO.\  
Maybe a more general approach will return in the future.

## [0.1.0] - 2020-12-20

### Added

- First schema version
- contains domain, options, and question catalog as root element
- Distribute JSON schema and validation function compiled with AJV

[Unreleased]: https://gitlab.com/rdmo-json/schema/compare/v0.8.3...main
[0.8.3]: https://gitlab.com/rdmo-json/schema/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/rdmo-json/schema/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/rdmo-json/schema/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/rdmo-json/schema/compare/v0.7.0...v0.8.0
[0.7.0]: https://gitlab.com/rdmo-json/schema/compare/v0.6.0...v0.7.0
[0.6.0]: https://gitlab.com/rdmo-json/schema/compare/v0.5.1...v0.6.0
[0.5.1]: https://gitlab.com/rdmo-json/schema/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/rdmo-json/schema/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/rdmo-json/schema/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/rdmo-json/schema/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/rdmo-json/schema/compare/v0.3.3...v0.4.0
[0.3.3]: https://gitlab.com/rdmo-json/schema/compare/v0.3.2...v0.3.3
[0.3.2]: https://gitlab.com/rdmo-json/schema/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/rdmo-json/schema/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/rdmo-json/schema/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/rdmo-json/schema/compare/v0.1.2...v0.2.0
[0.1.2]: https://gitlab.com/rdmo-json/schema/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/rdmo-json/schema/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/rdmo-json/schema/tags/v0.1.0

<!-- markdownlint-configure-file { "MD024": { "siblings_only": true }} -->
