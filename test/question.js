'use strict'
const test = require('ava')
const validate = require('../dist/validate-catalog')

function createCatalog () {
  return {
    prefix: 'https://rdmo.example.com',
    key: 'catalog_134_key',
    title: 'Catalog title',
    sections: [
      {
        key: 'section_key',
        title: 'Section title',
        questionsets: [
          {
            key: 'questionset_key',
            title: 'questionset title'
          }
        ]
      }
    ]
  }
}

function createQuestion () {
  return {
    key: 'question_key',
    comment: 'comment',
    text: 'Question text',
    help: 'Question help text',
    attribute: 'attribute'
  }
}

test.beforeEach(t => {
  t.context.data = createCatalog()
})

test('Yes/no question', t => {
  const question = {
    ...createQuestion(),
    type: 'yes-no'
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.attribute = null
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
  question.attribute = 'attribute'
  question.response = { format: 'text' }
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Textual question', t => {
  const question = {
    ...createQuestion(),
    type: 'single-response',
    response: { format: 'text' }
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.response.multiline = true
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.type = 'multiple-response'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Numeric question', t => {
  const question = {
    ...createQuestion(),
    type: 'single-response',
    response: { format: 'integer' }
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.response.range = [0, 5, 1]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.type = 'multiple-response'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.response.range = [0, 5]
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Date question', t => {
  const question = {
    ...createQuestion(),
    type: 'single-response',
    response: { format: 'date' }
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.type = 'multiple-response'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Single-choice question', t => {
  const question = {
    ...createQuestion(),
    type: 'single-choice',
    options: [
      { text: 'a' },
      { text: 'b', additionalInput: true }
    ]
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.style = 'dropdown'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.style = 'radio'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.options = null
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
  question.options = 'optionset'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Multiple-choice question', t => {
  const question = {
    ...createQuestion(),
    type: 'multiple-choice',
    options: [
      { key: 'key', text: 'a' },
      { text: 'b', comment: 'comment', additionalInput: true }
    ]
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.style = 'dropdown'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.style = 'checkbox'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  question.options = null
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
  question.options = 'optionset'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})

test('Multiple-response question with condition', t => {
  const question = {
    ...createQuestion(),
    type: 'multiple-response',
    conditions: [
      ['entity/attribute', 'eq', 'option', 'optionset/option']
    ]
  }
  t.context.data.sections[0].questionsets[0].questions = [question]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})
