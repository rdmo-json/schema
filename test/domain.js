'use strict'
const test = require('ava')
const validate = require('../dist/validate-catalog')

function createCatalog () {
  return {
    prefix: 'https://rdmo.example.com',
    key: 'catalog_134_key',
    title: 'Catalog title',
    sections: []
  }
}

test.beforeEach(t => {
  t.context.data = createCatalog()
})

test('Domain', t => {
  const domain = [
    'attribute',
    { entity: ['attribute'] }
  ]
  t.context.data.domain = domain
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  domain.push({ x: ['attribute'], y: ['attribute'] })
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
  domain.pop()
  domain.push({ x: 'attribute' })
  t.false(validate(t.context.data), JSON.stringify(validate.errors))
})
