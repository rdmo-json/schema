'use strict'
const test = require('ava')
const validate = require('../dist/validate-catalog')

function createCatalog () {
  return {
    prefix: 'https://rdmo.example.com',
    key: 'catalog_134_key',
    title: 'Catalog title',
    sections: [
      {
        key: 'section_key',
        title: 'Section title',
        questionsets: []
      }
    ]
  }
}

function createQuestionset () {
  return {
    key: 'questionset_key',
    comment: 'comment',
    title: 'questionset title',
    help: 'questionset help text'
  }
}

test.beforeEach(t => {
  t.context.data = createCatalog()
})

test('Questionset', t => {
  const questionset = {
    ...createQuestionset()
  }
  t.context.data.sections[0].questionsets = [questionset]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  questionset.conditions = [
    ['entity/attribute', 'neq', 'text', 'foo'],
    ['entity/attribute', 'neq', 'option', 'bar']
  ]
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
  questionset.attribute = 'entity/attribute'
  t.true(validate(t.context.data), JSON.stringify(validate.errors))
})
