'use strict'
const { writeFileSync } = require('fs')
const $RefParser = require('@apidevtools/json-schema-ref-parser')
const { compile } = require('json-schema-to-typescript')

$RefParser.bundle('./schema/catalog.yml')
  .then(schema => {
    writeFileSync('./dist/catalog.schema.json', JSON.stringify(schema))
    return compile(
      schema, 'Catalog',
      { bannerComment: '', style: { semi: false, singleQuote: true } }
    )
  })
  .then(ts => writeFileSync('./dist/types.d.ts', ts))
