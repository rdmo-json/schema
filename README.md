# RDMO JSON Schema

This project contains a JSON schema for maintaining [RDMO] catalogs.
The schema itself can be found under <https://rdmo-json.gitlab.io/schema/catalog.schema.json>.

## Quick start

If you want to write JSON documents conforming to my catalog schema,
I recommend you the following setup for productive writing:

1. Install and open [VSCode]
2. Create a new file and save it as JSON file, E.G. `catalog.json`
3. Insert the following snippet:

```json
{
  "$schema": "https://rdmo-json.gitlab.io/schema/catalog.schema.json",
}
```

If you continue typing after the $schema line,
VSCode will provide validation, formatting, navigation, and autocompletion
(see [JSON editing in Visual Studio Code] for more instructions).

If you are new to JSON, you can read [a short introduction to JSON](docs/json.md).

## Install

```shell
npm i @rdmo-author/schema
```

## Usage

The module has two exports:

- `schema`: The JSON schema itself
- `validate`: A validation function compiled by [Ajv]

[RDMO]: https://rdmorganiser.github.io
[VSCode]: https://code.visualstudio.com
[JSON editing in Visual Studio Code]: https://code.visualstudio.com/Docs/languages/json
[Ajv]: https://github.com/ajv-validator/ajv
