import { Schema, ValidateFunction } from 'ajv'
export * from './dist/types'
export const schema: Schema
export const validate: ValidateFunction;
