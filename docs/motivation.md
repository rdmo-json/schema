# Motivation

As part of my work, I had to create a structured interview for the [RDMO] platform.
A structured interview can be viewed as a relatively complex text document.
From a document author's viewpoint,
I found the authoring workflow very disruptive and unsatisfying.
In this project, I try to implement a workflow that fits my needs,
and I happily invite you to adopt it if it if you're convinced of it.

## My needs as a document editor

- Write down my thoughts focusing on the structure and content of the document,
  instead of filling in forms scattered accross the management console,
  having to maintain a prescribed editing order
  which distructs me and doesn't provide the experience of a document being created.
- Use my preferred text editor for productive writing.
- Draft my document offline and decide when it can be uploaded.
- Put my work under source control.
- It should be viably possible for humans to grasp and edit the content of the document,
  even if a running RDMO or my text editor are not accessible.
  RDMO exports XML documents, which is machine-readable and can be a good choice
  for describing templates or documents with a very dynamic structure
  where the structural information matters,
  but if the document structure is simple and static,
  it is human-unfriendly because the structural noise hides the content.
  A document structure for structural interviews could be statically defined,
  so a format which stresses content instead of structure
  might be more suited to fulfill this need.

## Conclusions

From the considerations above, I concluded the following steps to be done:

1. [x] Define a text document structure for structural interviews
   taking readability into account
2. [x] Make this definition machine-readable,
   so documents can be read and validated by software
3. [x] Implement software which can translate the read document
   to RDMO-compliant XML files for being imported into RDMO.
4. [ ] Maybe create more integration tooling for popular text editors

## Solution

Instead of inventing a new language,
I decided to view the interview documents as data documents
and defined a data schema that describes their structure.
From the existing languages which describe data documents (data serialization languages),
I have chosen JSON as the data language and created this JSON schema.

[RDMO]: https://rdmorganiser.github.io
