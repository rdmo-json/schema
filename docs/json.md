# Describing data with JSON

JSON is a text format for data representation, just like CSV describes tabular data.
Tabular representation puts more constraints on the data,
so for some data, JSON can be more appropriate.

## Which data can be represented with JSON?

A JSON document itself is of a data value.
There is a limited but powerful repertoire of value types:

- Scalar values
  - Number: `42`, `3.14`
  - Text surrounded by quotation marks: `"Lorem ipsum …"`
  - `true`, `false`, and `null` (means „nothing“)
- Structured values which can contain scalar and structured values themselves.
  - Ordered value lists surrounded by  brackets and with items delimited by commas:
    `["red", "green", "blue"]`
  - Unordered pair lists (records) surrounded by braces
    where each entry consists of a quoted key and a value separated by a colon:
    `{ "prefix": "https://rdmo.example.com/terms", "order": 0 }`

## A nested example

Because list items and pair values can also be structured values,
it is possible to describe hierarchical structures.
Space doesn't matter for JSON and can be inserted deliberately.
A question according to my schema can be written like the following example:

```json
{
  "key": "digital_data_kinds",
  "text": "Which kinds of digital data are you planning to use in your project?",
  "type": "multiple",
  "options": [
    { "text": "Images" },
    { "text": "Video files" },
    { "text": "Other data formats", "additionalInput": true },
  ]
}
```

- A question is a pair list.
- The value of the options entry is a list.
- Each item in the options list is a pair list
